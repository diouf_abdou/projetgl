package Model;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleResultSet;
import oracle.jdbc.OracleStatement;

public class User {
	
	private int id; 
	private String nom;
	private String email;

	
	public User(int id_user, String nom_user, String mail){
		
		this.id = id_user;
		this.nom = nom_user;
		this.email = mail;
		
	}
	
	public User(){
	}
	
	
	// ajoute un nouveau utilisateur	
		public void addTUser(User u) throws SQLException{
			
			Connection con = Outils.connecter();
			String requete = "insert into user_tweet values("+u.id+", '"+u.nom+"', '"+email+"')";
			OracleStatement st = (OracleStatement) con.createStatement();
			st.execute(requete);
			st.close();
			con.commit();
			con.close();
		}
		
	// supprime un utilisateur par son id	
		public void deleteUser(int id) throws SQLException{
			
			Connection con = (new Outils()).connecter();
			String requete = "delete from user_tweet where id ="+id;
			OracleStatement st = (OracleStatement) con.createStatement();
			st.execute(requete);
			st.close();
			con.commit();
			con.close();
		
		}
		
		
		// trouve un utilisateur par son id
		public User findUserById(int id ) throws SQLException{
			
			Connection con = (new Outils()).connecter();
			User user_tweet = null;
			OracleStatement st =(OracleStatement) con.createStatement();
			 OracleResultSet rs= (OracleResultSet)st.executeQuery("select * from user_tweet where user_id ="+id);
	        while(rs.next()){
	        	user_tweet = new User( rs.getInt(1), rs.getString(2), rs.getString(3));
	        }
	        st.close();
	        rs.close();
			con.close();
		
	        return user_tweet;
			
			
		}


}
