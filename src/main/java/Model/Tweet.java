package Model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleResultSet;
import oracle.jdbc.OracleStatement;

public class Tweet {

	private int id;
	private String content;
	private Date date_creation;
	private int user_id;

	
	public Tweet(int id_t, String contenu, Date date, int id_user){
		this.id = id_t;
		this.content = contenu;
		this.date_creation = date;
		this.user_id = id_user;
	}
	
	public Tweet(){

	}
	
// ajoute un nouveau tweet	
	public void addTweet(int id, String content, int user_id) throws SQLException{
		
		Connection con = Outils.connecter();
		String requete = "insert into tweet values("+id+", '"+content+"', SYSDATE, "+user_id+")";
		OracleStatement st = (OracleStatement) con.createStatement();
		st.execute(requete);
		st.close();
		con.commit();
		con.close();
	}
	
// supprime un tweet par son id	
	public void deleteTweet(int id) throws SQLException{
		
		Connection con = Outils.connecter();
		String requete = "delete from tweet where id ="+id;
		OracleStatement st = (OracleStatement) con.createStatement();
		st.execute(requete);
		st.close();
		con.commit();
		con.close();
	}
	
// cette fonction recupère tous les tweets d'un utilisateur
	public List<Tweet> getAllUserTweets(int id) throws SQLException{
		
		Connection con = Outils.connecter();
		List<Tweet> tweetslist = new ArrayList<Tweet>(); 
		
		 OracleStatement st =(OracleStatement) con.createStatement();
		 OracleResultSet rs= (OracleResultSet)st.executeQuery("select * from tweet where user_id ="+id);
		 while(rs.next()){
        	Tweet t = new Tweet( rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getInt(4));
        	tweetslist.add(t);
		 }
        st.close();
        rs.close();
		con.close();
        return tweetslist;
	}
	
	
	
}
