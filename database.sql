--création de la table user
create table user_tweet(
id integer,
nom varchar2(50), 
email varchar2(50),
constraint pk_user_tweet primary key(id));

--création de la table tweet

create table tweet(
id integer, 
content varchar2(200),
date_creation date,
user_id integer,
constraint pk_tweet primary key(id),
constraint fk_user_tweet foreign key(user_id) references user_tweet(id)
); 
